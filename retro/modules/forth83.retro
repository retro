MODULE: forth83
AUTHOR: Charles Childers
NOTES:  This provides partial support for the 1983 FORTH
NOTES:  standard. We can't achieve full compliance, since
NOTES:  the 1983 spec requires a 16-bit architecture, but
NOTES:  it can be useful as a learning tool, for old code,
NOTES:  and as a comparsion to more modern standards.

#! ----------------------------------------------------------

( ! )               ( Provided by RETRO )
( * )               ( Provided by RETRO )
( */ )              : */ push, *, pop, / ;          ' */ word: */
( */MOD )           : */MOD push, *, pop, /mod, ;   ' */MOD word: */MOD
( + )               ( Provided by RETRO )
( +! )              : +! dup, push, @, +, pop, !, ; ' +! word: +!
( - )               ( Provided by RETRO )
( / )               ( Provided by RETRO )
( /MOD )            ( Provided by RETRO )
( 0< )              : 0< 0 # <if -1 # ; then 0 # ;  ' 0< word: 0<
( 0= )              : 0= 0 # =if -1 # ; then 0 # ;  ' 0= word: 0=
( 0> )              : 0> 0 # >if -1 # ; then 0 # ;  ' 0> word: 0>
( 1+ )              ( Provided by RETRO )
( 1- )              ( Provided by RETRO )
( 2+ )              : 2+ 1+, 1+, ;  ' 2+ word: 2+
( 2- )              : 2- 1-, 1-, ;  ' 2- word: 2-
( 2/ )              : 2/ 1 # >>, ;  ' 2/ word: 2/
( < )               : < <if -1 # ; then 0 # ;  ' < word: <
( = )               : = =if -1 # ; then 0 # ;  ' = word: =
( > )               : > >if -1 # ; then 0 # ;  ' > word: >
( >R )              ' t-push macro: >R
( ?DUP )            : ?DUP dup, 0; ; ' ?DUP word: ?DUP
( @ )               ( Provided by RETRO )
( ABS )             : ABS dup, 0 # <if -1 # *, then ;  ' ABS word: ABS
( AND )             ' and word: AND
( C! )              ' ! word: C!
( C@ )              ' @ word: C@
( CMOVE )
( CMOVE> )
( COUNT )           : COUNT dup, @, swap, 1+, ;  ' COUNT word: COUNT
( D+ )
( D< )
( DEPTH )           ( Unsupported )
( DNEGATE )
( DROP )            ' drop word: DROP
( DUP )             ' dup word: DUP
( EXECUTE )         ' execute word: EXECUTE
( EXIT )            ' t-;; macro: EXIT
( FILL )
( I )
( J )
( MAX )             : MAX 2dup >if drop, ; then nip ;  ' MAX word: MAX
( MIN )             : MIN 2dup >if drop, ; then nip ;  ' MIN word: MIN
( MOD )             ' mod  word: MOD
( NEGATE )          ' neg  word: NEGATE
( NOT )             ' not  word: NOT
( OR )              ' or   word: OR
( OVER )            ' over word: OVER
( PICK )            ( Unsupported )
( R> )              ' t-pop macro: R>
( R@ )              : R@ pop, dup, push, ;
( ROLL )            ( Unsupported )
( ROT )             ' rot word: ROT
( SWAP )            ' swap word: SWAP
( U< )
( UM* )
( UM/MOD )
( XOR )             ' xor word: XOR
( BLOCK )
( BUFFER )
( CR )              ' cr word: CR
( EMIT )            ' emit word: EMIT
( EXPECT )
( FLUSH )
( KEY )             ' key word: KEY
( SAVE-BUFFERS )
( SPACE )           : SPACE 32 # emit ;  ' SPACE word: SPACE
( SPACES )          : SPACES repeat 0; SPACE 1-, again ; ' SPACES word: SPACES
( TYPE )
( UPDATE )
( # )
( #> )
( #S )
( #TIB )
( ' )               ( Provided by RETRO )
( ( )               ( Provided by RETRO )
( -TRAILING )
( . )
( .( )              : .( char: ) # accept tib # type ;   ' .( word: .(
( <# )
( >BODY )
( >IN )
( ABORT )
( BASE )
( BLK )
( CONVERT )
( DECIMAL )
( DEFINITIONS )
( FIND )
( FORGET )
( FORTH )            : FORTH nop, ;      ' FORTH    word: FORTH
( FORTH-83 )         : FORTH-83 nop, ;   ' FORTH-83 word: FORTH-83
( HERE )             ' t-here word: HERE
( HOLD )
( LOAD )
( PAD )              : PAD t-here 1024 # +, ;  ' PAD word: PAD
( QUIT ) 
( SIGN )
( SPAN )
( TIB )              tib data: TIB
( U. )
( WORD )
( +LOOP )
( , )                ( Provided by RETRO )
( ." )               : ." t-s" 7 # t-, ' type # t-, ;  ' ." macro: ."
( : )
( ; )                ( Provided by RETRO )
( ABORT" )
( ALLOT )
( BEGIN )            ' repeat macro: BEGIN
( COMPILE )
( CONSTANT )
( CREATE )           ' create word: CREATE
( DO )
( DOES> )
( ELSE )
( IF )
( IMMEDIATE )
( LEAVE )
( LITERAL )
( LOOP )
( REPEAT )
( STATE )            compiler data: STATE
( THEN )             ' t-then macro: THEN
( UNTIL )
( VARIABLE )         : VARIABLE create 0 # t-, ;  ' VARIABLE word: VARIABLE
( VOCABULARY )
( WHILE )
( ['] )              : ['] t-' 1 # t-, t-, ;  ' ['] macro: [']
( [COMPILE] )
( ] )                ( Provided by RETRO )
