/******************************************************
 * Ngaro
 *
 *|F|
 *|F| FILE: vm.c
 *|F|
 *
 * Threading interpreter by Matthias Schirm.
 * Released into the public domain
 ******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <termios.h>

#include "functions.h"
#include "vm.h"


/* Variables specific to the VM */
VM_STATE vm;



/******************************************************
 *|F| void init_vm()
 *
 * This zeros out everything in the VM_STATE struct
 * to give us a known starting point.
 ******************************************************/
void init_vm()
{
   int a;
   vm.ip = vm.sp = vm.rsp = 0;
   for (a = 0; a < STACK_DEPTH; a++)
      vm.data[a] = 0;
   for (a = 0; a < ADDRESSES; a++)
      vm.address[a] = 0;
   for (a = 0; a < IMAGE_SIZE; a++)
      vm.image[a] = 0;
   for (a = 0; a < 1024; a++)
      vm.ports[a] = 0;
}



void **vm_process_ct_otab[255];

void vm_process (void)
{
  int a, b, c;

  register int ip;   ip = vm.ip;
  register int sp;   sp = vm.sp;
  register int rsp; rsp = vm.rsp;

  register void *INST;

  register int *image   = vm.image;
  register int *data    = vm.data;
  register int *address = vm.address;

  register int IAC; IAC = 0;
  register int IOP; IOP = 0;

  vm_process_ct_otab[VM_NOP]       = &&fVM_NOP;
  vm_process_ct_otab[VM_LIT]       = &&fVM_LIT;
  vm_process_ct_otab[VM_DUP]       = &&fVM_DUP;
  vm_process_ct_otab[VM_DROP]      = &&fVM_DROP;
  vm_process_ct_otab[VM_SWAP]      = &&fVM_SWAP;
  vm_process_ct_otab[VM_PUSH]      = &&fVM_PUSH;
  vm_process_ct_otab[VM_POP]       = &&fVM_POP;
  vm_process_ct_otab[VM_CALL]      = &&fVM_CALL;
  vm_process_ct_otab[VM_JUMP]      = &&fVM_JUMP;
  vm_process_ct_otab[VM_RETURN]    = &&fVM_RETURN;
  vm_process_ct_otab[VM_GT_JUMP]   = &&fVM_GT_JUMP;
  vm_process_ct_otab[VM_LT_JUMP]   = &&fVM_LT_JUMP;
  vm_process_ct_otab[VM_NE_JUMP]   = &&fVM_NE_JUMP;
  vm_process_ct_otab[VM_EQ_JUMP]   = &&fVM_EQ_JUMP;
  vm_process_ct_otab[VM_FETCH]     = &&fVM_FETCH;
  vm_process_ct_otab[VM_STORE]     = &&fVM_STORE;
  vm_process_ct_otab[VM_ADD]       = &&fVM_ADD;
  vm_process_ct_otab[VM_SUB]       = &&fVM_SUB;
  vm_process_ct_otab[VM_MUL]       = &&fVM_MUL;
  vm_process_ct_otab[VM_DIVMOD]    = &&fVM_DIVMOD;
  vm_process_ct_otab[VM_AND]       = &&fVM_AND;
  vm_process_ct_otab[VM_OR]        = &&fVM_OR;
  vm_process_ct_otab[VM_XOR]       = &&fVM_XOR;
  vm_process_ct_otab[VM_SHL]       = &&fVM_SHL;
  vm_process_ct_otab[VM_SHR]       = &&fVM_SHR;
  vm_process_ct_otab[VM_ZERO_EXIT] = &&fVM_ZERO_EXIT;
  vm_process_ct_otab[VM_INC]       = &&fVM_INC;
  vm_process_ct_otab[VM_DEC]       = &&fVM_DEC;
  vm_process_ct_otab[VM_IN]        = &&fVM_IN;
  vm_process_ct_otab[VM_OUT]       = &&fVM_OUT;
  vm_process_ct_otab[VM_WAIT]      = &&fVM_WAIT;
  vm_process_ct_otab[VM_DEFAULT]   = &&fVM_DEFAULT;

  vm.image[IMAGE_SIZE] = VM_DEFAULT;

  NEXT

  fVM_NOP:	NEXT
  fVM_LIT:	data[++sp] = IOP;
		IOP = IAC;
                IAC = image[ip++];
         	NEXT
  fVM_DUP:	data[++sp] = IOP;
         	IOP = IAC;
         	NEXT
  fVM_DROP:	IAC = IOP;
		IOP = data[sp--];
         	NEXT
  fVM_SWAP:	a = IAC;
         	IAC = IOP;
         	IOP = a;
         	NEXT
  fVM_PUSH:	address[++rsp] = IAC;
         	IAC = IOP;
         	IOP = data[sp--];
         	NEXT
  fVM_POP:	data[++sp] = IOP;
		IOP = IAC;
         	IAC = address[rsp--];
         	NEXT
  fVM_CALL:	address[++rsp] = ip++;
         	ip = image[ip-1];
         	NEXT
  fVM_JUMP:	ip = image[ip];
         	NEXT
  fVM_RETURN:	ip = address[rsp--]+1;
         	NEXT
  fVM_GT_JUMP:	if(IOP > IAC)
		{
           	  ip = image[ip];
         	  IAC = data[sp--];
		  IOP = data[sp--];
		  NEXT
		}
         	ip++;
		IAC = data[sp--];
		IOP = data[sp--];
		NEXT
  fVM_LT_JUMP:	if(IOP < IAC)
		{
           	  ip = image[ip];
         	  IAC = data[sp--];
		  IOP = data[sp--];
         	  NEXT
		}
		ip++;
		IAC = data[sp--];
		IOP = data[sp--];
		NEXT
  fVM_NE_JUMP:	if(IAC != IOP)
		{
           	  ip = image[ip];
         	  IAC = data[sp--];
		  IOP = data[sp--];
         	  NEXT
		}
		ip++;
		IAC = data[sp--];
		IOP = data[sp--];
		NEXT
  fVM_EQ_JUMP:	if(IAC == IOP)
		{
           	  ip = image[ip];
         	  IAC = data[sp--];
		  IOP = data[sp--];
         	  NEXT
		}
		ip++;
		IAC = data[sp--];
		IOP = data[sp--];
		NEXT
  fVM_FETCH:	IAC = image[IAC];
         	NEXT
  fVM_STORE:	image[IAC] = IOP;
         	IAC = data[sp--];
		IOP = data[sp--];
         	NEXT
  fVM_ADD:	IOP += IAC;
		IAC = IOP;
         	IOP = data[sp--];
         	NEXT
  fVM_SUB:	IOP -= IAC;
		IAC = IOP;
         	IOP = data[sp--];
         	NEXT
  fVM_MUL:	IOP *= IAC;
		IAC = IOP;
         	IOP = data[sp--];
         	NEXT
  fVM_DIVMOD:	a = IAC;
         	b = IOP;
         	IAC = b / a;
         	IOP = b % a;
         	NEXT
  fVM_AND:	IAC = IAC & IOP;
		IOP = data[sp--];
         	NEXT
  fVM_OR:	IAC = IAC | IOP;
		IOP = data[sp--];
         	NEXT
  fVM_XOR:	IAC = IAC ^ IOP;
		IOP = data[sp--];
         	NEXT
  fVM_SHL:	IAC = IOP << IAC;
		IOP = data[sp--];
         	NEXT
  fVM_SHR:	IAC = IOP >>= IAC;
		IOP = data[sp--];
         	NEXT
  fVM_ZERO_EXIT:if (IAC == 0)
         	{
           	  IAC = IOP;
		  IOP = data[sp--];
           	  ip = address[rsp--]+1;
         	}
         	NEXT
  fVM_INC:	IAC += 1;
         	NEXT
  fVM_DEC:	IAC -= 1;
         	NEXT
  fVM_IN:	a = IAC;
         	IAC = vm.ports[a];
         	vm.ports[a] = 0;
         	NEXT
  fVM_OUT:	vm.ports[0] = 0;
         	vm.ports[IAC] = IOP;
         	IAC = data[sp--];
		IOP = data[sp--];
         	NEXT
  fVM_WAIT:
                if (vm.ports[0] == 0 && vm.ports[1] == 1)
  		{
    		  vm.ports[1] = getchar();
    		  vm.ports[0] = 1;
  		}
  		if (vm.ports[2] == 1)
  		{
    		  c = IAC;
		  IAC = IOP;
		  IOP = data[sp--];
    		  draw_character(c);
    		  vm.ports[2] = 0;
    		  vm.ports[0] = 1;
  		}
  		if (vm.ports[4] == 1)
  		{
    		  vm_save_image(vm.filename);
    		  vm.ports[4] = 0;
    		  vm.ports[0] = 1;
  		}
		/* Capabilities */
		if (vm.ports[5] == -1)
		{
		  vm.ports[5] = IMAGE_SIZE;		  vm.ports[0] = 1;
		}
		if (vm.ports[5] == -2 || vm.ports[5] == -3 || vm.ports[5] == -4)
		{
		  vm.ports[5] = 0;
		  vm.ports[0] = 1;
		}
         	NEXT
  fVM_DEFAULT:	vm.ip  = ip;
		vm.sp  = sp;
		vm.rsp = rsp;
}
