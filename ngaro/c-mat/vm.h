#ifndef NGARO_HEADER
#define NGARO_HEADER

enum vm_opcode {VM_NOP, VM_LIT, VM_DUP, VM_DROP, VM_SWAP, VM_PUSH, VM_POP,
                VM_CALL, VM_JUMP, VM_RETURN, VM_GT_JUMP, VM_LT_JUMP,
                VM_NE_JUMP,VM_EQ_JUMP, VM_FETCH, VM_STORE, VM_ADD,
                VM_SUB, VM_MUL, VM_DIVMOD, VM_AND, VM_OR, VM_XOR, VM_SHL,
                VM_SHR, VM_ZERO_EXIT, VM_INC, VM_DEC, VM_IN, VM_OUT,
                VM_WAIT, VM_DEFAULT};

#define CELL int
#define DROP vm.data[vm.sp] = 0; vm.sp--;
#define TOS  vm.data[vm.sp]
#define NOS  vm.data[vm.sp-1]
#define TORS vm.address[vm.rsp]
#define NORS vm.address[vm.rsp-1]

#define drop data[sp] = 0; sp--;
#define tos  data[sp]
#define nos  data[sp-1]
#define tors address[rsp]
#define nors address[rsp-1]

#define IMAGE_SIZE   5000000
#define STACK_DEPTH      100
#define ADDRESSES       1024

#define NEXT goto *INST = vm_process_ct_otab[vm.image[ip++]];

typedef struct {
  int sp;
  int rsp;
  int ip;
  int data[STACK_DEPTH];
  int address[ADDRESSES];
  int ports[1024];
  int image[IMAGE_SIZE];
  /* This is needed to avoid corruption of the file name. A bug? */
  int padding;
  char filename[2048];
} VM_STATE;

#endif
